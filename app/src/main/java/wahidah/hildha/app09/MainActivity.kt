package wahidah.hildha.app09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    val daftarlagu = intArrayOf(R.raw.musik1, R.raw.musik2, R.raw.musik3)
    val daftarjudul = arrayOf("Bright as The Sun", "Everything I Need", "A Whole New World")
    val daftarcover = intArrayOf(R.drawable.cover1, R.drawable.cover2, R.drawable.cover3)

    var id_mus  =0
    var id_covr = 0
    var judul = ""


    lateinit var db: SQLiteDatabase
    lateinit var lsAdapter: ListAdapter


    var posLaguSkrg = 0
    var durasiLagu = 0
    var handler = Handler()

    lateinit var  mediaPlayer: MediaPlayer
    lateinit var  mediaController: MediaController



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max=100
        seekSong.progress= 0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        db = DBOpenHelper(this).writableDatabase
        ReadDataMusic()
        listMusic.setOnItemClickListener(OnClickListView)
    }

    fun ReadDataMusic() {
        var sql = "select id_music as _id, id_cover, music_title from musik"
        val cursor : Cursor = db.rawQuery(sql, null)
        lsAdapter =
            SimpleCursorAdapter(
                this, R.layout.list_music, cursor,
                arrayOf("_id", "id_cover","music_title"), intArrayOf(R.id.id_music, R.id.id_cover, R.id.music_title),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        listMusic.adapter = lsAdapter
    }

    val OnClickListView = AdapterView.OnItemClickListener{parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        judul = c.getString(c.getColumnIndex("music_title"))

        audioPlay(c.position)
    }




    fun milliSecondToString(ms : Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun  audioPlay (pos : Int){
        mediaPlayer = MediaPlayer.create(this, daftarlagu[pos])
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(daftarcover[pos])
        txJudulLagu.setText(daftarjudul[pos])
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread, 50)
    }

    fun audioNext() {
        if (mediaPlayer.isPlaying)mediaPlayer.stop()
        if (posLaguSkrg<(daftarlagu.size-1)) {
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        audioPlay(posLaguSkrg)
    }

    fun audioPrev() {
        if (mediaPlayer.isPlaying)mediaPlayer.stop()
        if (posLaguSkrg>0) {
            posLaguSkrg--
        }else {
            posLaguSkrg = daftarlagu.size-1
        }
        audioPlay(posLaguSkrg)
    }

    fun audioStop() {
        if (mediaPlayer.isPlaying)mediaPlayer.stop()

    }

    inner class UpdateSeekBarProgressThread : Runnable {
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress= currTime
            if (currTime != mediaPlayer.duration) handler.postDelayed(this, 50)
        }
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay -> {
                audioPlay(posLaguSkrg)

            }
            R.id.btnNext -> {
                audioNext()

            }
            R.id.btnPrev -> {
                audioPrev()

            }
            R.id.btnStop -> {
                audioStop()

            }
        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it)}
    }
}
