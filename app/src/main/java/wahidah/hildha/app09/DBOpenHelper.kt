package wahidah.hildha.app09


import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context) : SQLiteOpenHelper(context, DB_Name, null, DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tMusic =
            "create table musik(id_music int primary key, id_cover int not null, music_title text)"
        val tMusicInsert =
            "insert into musik(id_music, id_cover, music_title) values ('0x7f0c0000','0x7f06005f','Bright As The Sun'), ('0x7f0c0001','0x7f060060','Everything I Need'), ('0x7f0c0002','0x7f060061','A Whole New World')"
        db?.execSQL(tMusic)
        db?.execSQL(tMusicInsert)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object {
        val DB_Name = "Music"
        val DB_Ver = 1
    }
}

